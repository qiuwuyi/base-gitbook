# Summary

* [前言](README.md)
* [strings 字符串](Chapter1/readme.md)
    * [Compare 比较](Chapter1/Compare.md)
    * [Contains 包含](Chapter1/Contains.md)
    * [ContainsAny 包含](Chapter1/ContainsAny.md)
    * [ContainsRune 包含](Chapter1/ContainsRune.md)
    * [Count 数个数](Chapter1/Count.md)
    * [EqualFold 小写相同](Chapter1/EqualFold.md)
    * [Fields 空格拆分](Chapter1/Fields.md)
    * [FieldsFunc 函数拆分](Chapter1/FieldsFunc.md)

