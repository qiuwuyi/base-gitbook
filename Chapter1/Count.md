# Count 数个数

## 类型

```go
func Count(s, substr string) int
```

## 说明

Count counts the number of non-overlapping instances of substr in s. If substr is an empty string, Count returns 1 + the number of Unicode code points in s.

Count 数出 s 中和 substr 相同片段的次数, 如果 substr 是空字符串, Count 返回 1 + s 的 unicode 字符数

## 用例

```go
package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Count("cheese", "e"))
	fmt.Println(strings.Count("five", "")) // before & after each rune
}
```

## 源码

```go
func Count(s, substr string) int {
	// special case
	if len(substr) == 0 {
		return utf8.RuneCountInString(s) + 1
	}
	if len(substr) == 1 {
		return bytealg.CountString(s, substr[0])
	}
	n := 0
	for {
		i := Index(s, substr)
		if i == -1 {
			return n
		}
		n++
		s = s[i+len(substr):]
	}
}
```

## ps

不是很认同这里空字符串的处理
