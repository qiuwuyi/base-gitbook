# ContainsRune 包含

## 类型

func ContainsRune(s string, r rune) bool

## 说明

ContainsRune reports whether the Unicode code point r is within s.

ContainsRune 报告 Unicode 码 r(对应的字符) 是否在 s 中

## 用例

```go
package main

import (
	"fmt"
	"strings"
)

func main() {
	// Finds whether a string contains a particular Unicode code point.
	// The code point for the lowercase letter "a", for example, is 97.
	fmt.Println(strings.ContainsRune("aardvark", 97)) // true
	fmt.Println(strings.ContainsRune("timeout", 97)) // false
}
```

## 源码

```go
func ContainsRune(s string, r rune) bool {
	return IndexRune(s, r) >= 0
}

// IndexRune returns the index of the first instance of the Unicode code point
// r, or -1 if rune is not present in s.
// If r is utf8.RuneError, it returns the first instance of any
// invalid UTF-8 byte sequence.
func IndexRune(s string, r rune) int {
	switch {
	case 0 <= r && r < utf8.RuneSelf:
		return IndexByte(s, byte(r))
	case r == utf8.RuneError:
		for i, r := range s {
			if r == utf8.RuneError {
				return i
			}
		}
		return -1
	case !utf8.ValidRune(r):
		return -1
	default:
		return Index(s, string(r))
	}
}
```

## ps

大量的 utf8 包的内容, 咕咕咕
