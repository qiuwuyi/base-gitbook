# Fields 空格拆分

## 类型

```go
func Fields（s string）[] string
```

## 说明

Fields splits the string s around each instance of one or more consecutive white space characters, as defined by unicode.IsSpace, returning a slice of substrings of s or an empty slice if s contains only white space.

(不是翻译)Fields 将字符串 s 按空格(像 unicode.IsSpace 定义的那样)拆分为一个或多个连续字符, 返回 s 的子字符串片段或空片段（如果 s 仅包含空格）。

## 用例

```go

package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Printf("Fields are: %q", strings.Fields("  foo bar  baz   ")) // Fields are: ["foo" "bar" "baz"]
	fmt.Printf("Fields are: %q", strings.Fields("     ")) // Fields are: []
	fmt.Printf("Fields are: %q", strings.Fields("")) // Fields are: []
}


```

## 源码

```go
func Fields(s string) []string {
	// First count the fields.
	// This is an exact count if s is ASCII, otherwise it is an approximation.
	n := 0
	wasSpace := 1
	// setBits is used to track which bits are set in the bytes of s.
	setBits := uint8(0)
	for i := 0; i < len(s); i++ {
		r := s[i]
		setBits |= r
		isSpace := int(asciiSpace[r])
		n += wasSpace & ^isSpace
		wasSpace = isSpace
	}

	if setBits < utf8.RuneSelf { // ASCII fast path
		a := make([]string, n)
		na := 0
		fieldStart := 0
		i := 0
		// Skip spaces in the front of the input.
		for i < len(s) && asciiSpace[s[i]] != 0 {
			i++
		}
		fieldStart = i
		for i < len(s) {
			if asciiSpace[s[i]] == 0 {
				i++
				continue
			}
			a[na] = s[fieldStart:i]
			na++
			i++
			// Skip spaces in between fields.
			for i < len(s) && asciiSpace[s[i]] != 0 {
				i++
			}
			fieldStart = i
		}
		if fieldStart < len(s) { // Last field might end at EOF.
			a[na] = s[fieldStart:]
		}
		return a
	}

	// Some runes in the input string are not ASCII.
	return FieldsFunc(s, unicode.IsSpace)
}


```

## ps

首先判断是不是 ASCII
然后挺蠢的啊...要先判断被切成几段, 然后再遍历一次放进去
虽然理论上一次遍历的效率更高, 可能先遍历一次确定数组个数节省内存?
