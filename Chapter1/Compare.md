# Compare

## 类型

```go
func Compare(a, b string) int
```

## 说明

Compare returns an integer comparing two strings lexicographically. The result will be 0 if a==b, -1 if a < b, and +1 if a > b.

Compare is included only for symmetry with package bytes. It is usually clearer and always faster to use the built-in string comparison operators ==, <, >, and so on.

通过比较两个字符串的字典顺序, compare 返回一个数字. 如果 a == b, 结果为 0, a < b 为-1, a > b 为 1.

## 用例

```go
package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Compare("a", "b")) // -1
	fmt.Println(strings.Compare("a", "a")) // 0
	fmt.Println(strings.Compare("b", "a")) // 1
	fmt.Println(strings.Compare("ab", "a")) // 1
}
```

## 源码

```go
func Compare(a, b string) int {
	if a == b {
		return 0
	}
	if a < b {
		return -1
	}
	return +1
}
```

```
NOTE(rsc):
This function does NOT call the runtime cmpstring function,
because we do not want to provide any performance justification for
using strings.Compare. Basically no one should use strings.Compare.

As the comment above says, it is here only for symmetry with package bytes.
If performance is important,
the compiler should be changed to recognizethe pattern
so that all code doing three-way comparisons,
not just code using strings.Compare, can benefit.
```

## ps

我不是很能理解说明中这个方法总是比用<快这个说法, 从源码看, 内部是用的< ???

这个方法让我想起 php 的太空船比较符, 我想<=>这个符号更 clean

Compare is included only for symmetry with package bytes.看不懂这句

源码中的注释也看不明白啥意思
