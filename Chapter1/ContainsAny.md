# ContainsAny 包含

## 类型

```
func ContainsAny(s, chars string) bool
```

## 说明

ContainsAny reports whether any Unicode code points in chars are within s.

ContainsAny 报告 chars 中是否存在任意一个在 s 中的字符

## 用例

```go
package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.ContainsAny("team", "i")) // false
	fmt.Println(strings.ContainsAny("failure", "u & i")) // true
	fmt.Println(strings.ContainsAny("foo", "")) // false
	fmt.Println(strings.ContainsAny("", "")) // false
}
```

## 源码

```go
func ContainsAny(s, chars string) bool {
	return IndexAny(s, chars) >= 0
}

func IndexAny(s, chars string) int {
	if chars == "" {
		// Avoid scanning all of s.
		return -1
	}
	if len(s) > 8 {
		if as, isASCII := makeASCIISet(chars); isASCII {
			for i := 0; i < len(s); i++ {
				if as.contains(s[i]) {
					return i
				}
			}
			return -1
		}
	}
	for i, c := range s {
		for _, m := range chars {
			if c == m {
				return i
			}
		}
	}
	return -1
}
```

## ps

IndexAny 这个方法为什么没有先比较 s 和 chars 的长度, 选一个较长的做表呢
