# Contains 包含

## 类型

```go
func Contains(s, substr string) bool
```

## 说明

Contains reports whether substr is within s.

Contains 报告 substr 中是否在 s 中

## 举例

```go
package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Contains("seafood", "foo")) // true
	fmt.Println(strings.Contains("seafood", "bar")) // false
	fmt.Println(strings.Contains("seafood", "")) // true
	fmt.Println(strings.Contains("", "")) // true
}
```

## 源码

```go
func Contains(s, substr string) bool {
	return Index(s, substr) >= 0
}
```

## ps

Index 这个函数还挺复杂, 简单解释是按照 substr 的长度从短到长分别判断的(这个说法不严谨, 严格说是判断的容易程度)等以后看基本包的时候(tan90)再仔细解释这个函数
