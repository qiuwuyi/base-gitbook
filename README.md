# 前言

## 内容

## 作者

qsoft

## 起始编写日期

2018-11-13

## 命令

| 行为      | script     |
| --------- | ---------- |
| 适应目录  | yarn i     |
| 调试编写  | yarn serve |
| 打包 pdf  | yarn pdf   |
| 打包 html | yarn html  |

## 安装 ebook 以打包 pdf

---

mac

1. https://calibre-ebook.com/下载
2. 拖入 app 文件夹
3. sudo ln -s /Applications/calibre.app/Contents/MacOS/ebook-convert /usr/local/bin

---

windows

我不知道...

---

```

```
